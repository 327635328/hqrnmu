import React, {Component} from 'react';
import { View,Text,StyleSheet } from "react-native";

type Props = {};
export default class CodePushPage extends Component<Props> {




  render() {
    return <View style={styles.container}>
      <Text style={styles.text}>{'CodePushPage'}</Text>
    </View>;
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  }
});
