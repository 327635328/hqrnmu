/*
 * @Author: leeym
 * @Date: 2021-05-17 10:28:23
 * @Last Modified by: Leeym
 * @Description: undefined
 */
import QS from 'qs';
// import {changeLoginInfo} from '../store/user/actions';
// import {storage_removeItem} from '../utils/storage';
// import {SERVER_URL} from '../config/config';
//
// import {queryDataType} from '../utils/utils';
// import {encrypt, decrypt} from '../utils/cryptoMethods';
//
// import otherApi from '../service/api/other';
//
// import {Platform} from 'react-native';
//
// import {getAppDetails} from '../utils/getAppInfomation';

export const $ajax = (
  {dispatch, navigation}, // 必传参数
  url = '', // 必传参数
  params = {},
  method = 'POST',
  dataFormat = 'json',
  urlData = {},
  headers = {},
) =>
  new Promise(async (resolve, reject) => {
    if (!global.appInfo) await getAppDetails();

    const config = {
      headers: {
        version:
          global.appInfo && global.appInfo.appVersion
            ? global.appInfo.appVersion
            : '1.0.0',
        channelCode: Platform.OS === 'ios' ? 'mb-ios' : 'mb-android',
        'Content-Type': 'application/json',
        Connection: 'close',
        Authorization: global.token || '',
      },
      method,
    };

    if (headers != {}) config.headers = {...config.headers, ...headers};

    if (method !== 'GET') config.body = JSON.stringify(params);

    if (method !== 'GET' && dataFormat === 'form') {
      config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      config.body = QS.stringify(params);
    }

    if (dataFormat === 'formData')
      config.headers['Content-Type'] = 'multipart/form-data';

    if (method === 'GET' || (urlData && Object.keys(urlData).length > 0)) {
      const urlParams = method === 'GET' ? params : urlData;

      for (const urlParamsKey in urlParams) {
        url +=
          url.indexOf('?') <= -1
            ? `?${urlParamsKey}=${urlParams[urlParamsKey]}`
            : `&${urlParamsKey}=${urlParams[urlParamsKey]}`;
      }
    }

    // 上传文件
    if (dataFormat === 'file') {
      const formData = new FormData();
      for (const key in params) {
        if (key == 'files') {
          params.files.forEach(item => {
            let file = {
              uri: item.uri || '',
              name: item.fileName ? item.fileName : '',
              type: item.type ? item.type : 'multipart/form-data',
            };
            formData.append(params.formDataKey || 'files', file);
          });
        } else {
          formData.append(key, params[key]);
        }
      }

      config.headers['Content-Type'] = 'multipart/form-data';
      config.body = formData;
    }

    // 加密入参iem
    if (
      // (global.customServiceIP === 'http://10.242.31.155:9999' ||
      //   SERVER_URL === 'http://10.242.31.155:9999') &&
      method === 'POST' &&
      config.headers['Content-Type'] === 'application/json' &&
      url.indexOf('/auth') <= -1 &&
      url !== otherApi.dataBuriedPoint
    ) {
      const {body} = config;

      console.log('加密前接口入参=========================', body, url);

      config.body = JSON.stringify({
        data:
          !body ||
          body === '{}' ||
          (queryDataType(body) === 'Object' && Object.keys(body).length <= 0) ||
          body === '[]' ||
          (queryDataType(body) === 'Array' && body.length <= 0)
            ? ''
            : encrypt(body),
        timestamp: new Date().getTime(),
        sign: '',
      });

      console.log(
        '解密后接口入参=========================',
        decrypt(JSON.parse(config.body).data),
        url,
      );
    }

    console.log('config===', config);

    fetch(`${global.customServiceIP || SERVER_URL}${url}`, config)
      // fetch(`http://10.242.31.155:9998${url}`, config)
      .then(res => {
        console.log(
          'service-res',
          res,
          (global.customServiceIP || SERVER_URL) + url,
        );

        if (res.ok) {
          let data = null;

          switch (dataFormat) {
            case 'text':
              data = res.text();
              break;
            case 'formData':
              data = res.formData();
              break;
            case 'fileStream':
              data = res.blob();
              break;
            case 'binary':
              data = res.arrayBuffer();
              break;
            default:
              data = res.json();
              break;
          }

          return data;
        } else {
          let errMsg = '';
          switch (res.status) {
            case 401: // '登录过期'
              errMsg = '请先登录';

              global.accessToken = '';
              global.token = '';

              dispatch(
                changeLoginInfo({
                  isLogin: false,
                }),
              ); // 更新登录态

              storage_removeItem('loginInfo'); // 清除缓存中的登录信息
              storage_removeItem('userInfo'); // 清除缓存中的用户信息

              // UserInfo.clearUserInfo();

              setTimeout(() => {
                navigation.navigate('Login'); // 直接跳转至登录页面
              }, 2000);

              break;
            case 403: // 服务器拒绝响应
              errMsg = '服务器拒绝响应';
              break;
            case 404: // 网络请求不存在
              errMsg = '网络请求不存在';
              break;
            case 504: // 服务器内部异常
              errMsg = '服务器内部异常';
              break;
            default:
              // 显示：接口返回错误信息 or 服务器异常
              errMsg = '服务器异常';
              break;
          }

          global.toast(errMsg);

          reject(res);
        }
      })
      .then(res => {
        console.log(
          '哈哈哈哈哈哈==========res============',
          res,
          (global.customServiceIP || SERVER_URL) + url,
        );
        // 上面then中res.ok条件不成立时，res值为undefined，调用$ajax方法catch中获取的异常数据是上面then方法中reject抛出的
        if (!res) {
          return false;
        }

        //登录接口特殊处理
        if (res && res.access_token) {
          let resInfo = {
            code: 0,
            msg: '成功',
            data: res,
          };

          res = resInfo;
        }

        if (res.code !== 0) {
          if (res.code === 666) return false; //微信登录返回666为游客身份，此提示不用弹出
          global.toast(res.msg || '请求异常，请稍后重试...');

          return reject(res);
        }

        // 解密回参
        if (
          // (global.customServiceIP === 'http://10.242.31.155:9999' ||
          //   SERVER_URL === 'http://10.242.31.155:9999') &&
          // config.headers['Content-Type'] === 'application/json' &&
          url.indexOf('/auth') <= -1 &&
          url !== otherApi.dataBuriedPoint
        ) {
          let resData = null;

          if (res.data) {
            try {
              resData = JSON.parse(decrypt(res.data));
            } catch (err) {
              resData = res.data;
            }
          } else {
            resData = queryDataType(res.data) !== 'Undefined' ? res.data : {};
          }

          res.data = resData;

          console.log('解密后数据=====================', res);
        }

        resolve(res);
      })
      .catch(err => {
        if (err.toString() === 'TypeError: Network request failed')
          global.toast('无网络，请检查网络连接');

        reject(err);
      });
  });
