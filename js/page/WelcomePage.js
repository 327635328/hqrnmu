import React, {Component} from 'react';
import { View, Text, StyleSheet, Button } from "react-native";
import NavigationUtil from "../navigator/NavigationUtil";

type Props = {};
export default class WelcomePage extends Component<Props> {

  constructor(props) {
    super(props);
    console.log(props);
    // console.log(props.route.name);
  }


  componentDidMount() {
    this.timer = setTimeout(()=>{
      NavigationUtil.resetToHomPage({
        navigation: this.props.navigation,
      })
    },1000);

  }

  componentWillUnmount() {
    this.timer && clearTimeout(this.timer);
  }

  render() {
    return <View style={styles.container}>
      <Text style={styles.text}>{'this.props.route.params.name'}</Text>
      <Button
        title={"跳转"}
        onPress={()=>{
          this.props.navigation.navigate("AboutPage")
        }}
      >

      </Button>
    </View>;
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  }
});
