import React, { Component, lazy } from "react";
import {NavigationContainer} from '@react-navigation/native';
import {BottomTabBar, createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PopularPage from "../page/NavigatorSubs/PopularPage";
import TrendingPage from "../page/NavigatorSubs/TrendingPage";
import FavoritePage from "../page/NavigatorSubs/FavoritePage";
import MyPage from "../page/NavigatorSubs/MyPage";
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import {LogBox} from 'react-native'
import EventBus from "react-native-event-bus";
import EventTypes from "../util/EventTypes";



const Tab = createBottomTabNavigator();

type Props = {};


const TABS = {//在这里配置页面的路由
  PopularPage: {
    screen: PopularPage,
    navigationOptions: {
      tabBarLabel: '最热',
      tabBarIcon: ({color, focused}) => (
        <MaterialIcons
          name={'whatshot'}
          size={26}
          style={{color: color}}
        />
      ),
    },
  },
  TrendingPage: {
    screen: TrendingPage,
    navigationOptions: {
      tabBarLabel: '趋势',
      tabBarIcon: ({color, focused}) => (
        <Ionicons
          name={'md-trending-up'}
          size={26}
          style={{color: color}}
        />
      ),
    },
  },
  FavoritePage: {
    screen: FavoritePage,
    navigationOptions: {
      tabBarLabel: '收藏',
      tabBarIcon: ({color, focused}) => (
        <MaterialIcons
          name={'favorite'}
          size={26}
          style={{color: color}}
        />
      ),
    },
  },
  MyPage: {
    screen: MyPage,
    navigationOptions: {
      tabBarLabel: '我的',
      tabBarIcon: ({color, focused}) => (
        <Entypo
          name={'user'}
          size={26}
          style={{color: color}}
        />
      ),
    },
  },
};




export default class DynamicTabNavigator extends Component<Props> {
  constructor(props) {
    super(props);
    LogBox.ignoreAllLogs('')//关闭黄色警告弹窗
  }


  //底部tab点击事件
  fireEvent(navigationState) {

    const {index, history, routeNames} = navigationState;
    if (history.length === 1) {
      return;
    }
    let fromIndex;
    let key = history[history.length - 2].key;
    for (let i = 0; i < routeNames.length; i++) {
      if (key.startsWith(routeNames[i])) {
        fromIndex = i;
        break;
      }
    }
    EventBus.getInstance().fireEvent(EventTypes.bottom_tab_select, {//发送底部tab切换的事件
      from: fromIndex,
      to: index,
    });

  }

  _tabNavigator() {
    if (this.Tabs) {
      return this.Tabs;
    }
    const {PopularPage, TrendingPage, FavoritePage, MyPage} = TABS;
    const tabs = {PopularPage, TrendingPage, FavoritePage, MyPage};//根据需要定制显示的tab
    PopularPage.navigationOptions.tabBarLabel = '热门';//动态配置Tab属性
    // debugger
    //
    // console.log(Object.entries(tabs))
    // debugger
    return this.Tabs = <NavigationContainer
      independent={true}
    >
      <Tab.Navigator
        // screenOptions={{headerShown: false}}
        tabBar={props => {
          this.fireEvent(props.state);
          return <TabBarComponent {...props}/>;
        }}
      >
        {
          Object.entries(tabs).map((item,index) => {
            return <Tab.Screen
              name={item[0]}
              component={item[1].screen}
              options={item[1].navigationOptions}
              key = {index}
            />;
          })

        }
      </Tab.Navigator>
    </NavigationContainer>;
  }

  render() {
    const Tab = this._tabNavigator();
    return Tab;
  }
}

class TabBarComponent extends React.Component {
  constructor(props) {
    super(props);
    this.theme = {
      tintColor: props.activeTintColor,
      updateTime: new Date().getTime(),
    };
  }

  render() {
    return <BottomTabBar
      {...this.props}
    />;
  }
}
