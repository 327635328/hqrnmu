import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, Button, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { styles } from "../../service/serviceStyles";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import { scale } from "react-native-size-matters";
import { useDispatch, useSelector } from "react-redux";
import { changeIsShowLogin, changeLoginInfo, changeSetUserInfo, changeUserInfo } from "../../store/user/actions";
import { healthDataColors } from "../../service/serviceManager";
import { healthRecordRight } from "../../service/serviceView";
import serviceHeaderView from "../../service/serviceHeaderView";


const touchableOpacityAlpha = 0.6;
const Service = props => {

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const {
    isLogin = false,
    isVx = false,
    userInfo = {},
  } = useSelector(state => ({
    isLogin: state.user.isLogin,
    isVx: state.user.isVx,
    userInfo: state.user.userInfo,
  }));

  const [healthGradeInfo, setHealthGradeInfo] = useState({ grade: 0, gradeEarly: '', }); // 健康指数
  const [healthColor, setHealthColor] = useState('#67C6D5'); // 健康档案值颜色
  const [currentBlood, setCurrentBlood] = useState({ inputToday: false, describe: '', dpd: '0', sbp: '0' }); // 血压
  const [heartRate, setHeartRate] = useState({ inputToday: false, describe: '', heartNumber: 0 }); // 心率
  const [weightInfo, setWeightInfo] = useState({ weight: 0, describe: '', }); // 体重
  const [sleepData, setSleepData] = useState({ sleepTime: 0, status: 0, describe: ''}); //睡眠
  const [circleList, setCircleList] = useState([]); // 热门圈子
  const [goodsList, setGoodsList] = useState([]); // 好物商品列表
  const [categoryList, setCategoryList] = useState([]); // 健康资讯
  const [liking, setLiking] = useState(false); // 是否点赞中
  const [activeTab, setActiveTab] = useState(0);
  const [consume, setConsume] = useState(0); // 运动消耗热量
  const [dietInfo, setDietInfo] = useState({ energyCount: 0,adviceEnergy: 0}); // 饮食详情 [当天摄入的食物总能量,推荐摄入量]
  const [recommendFoods, setRecommendFoods] = useState([]); // 饮食推荐数据
  const [recommendSports, setRecommendSports] = useState({}); // 运动推荐数据
  const [userHealthData, setUserHealthData] = useState({}); // 用户已录入的健康资料
  const [haveHealthData, setHaveHealthData] = useState(false); // 用户是否已录入所有必填的健康资料数据

  useEffect(() => {
    // dataBuriedPoint();
    // nothingBuriedPoint('100000000', 'enter', []);
    getRecommendFoods();
    // getRecommendSports();
  }, []);


  const getRecommendFoods = () =>{
    new Promise(resolve=>
      {

      }

    )
  }


  const noLoginFun = () =>{

  }


  const headerView = () => {
    // if (this.headerView){
    //   return this.headerView
    // }
    return (
      this.headerView = <View style={styles.header}>
        <View style={styles.titleCon}>
          <Text style={styles.title}>个人健康</Text>
          <TouchableOpacity
            style={styles.headerTouch}
            onPress={() => {
              // dispatch(changeIsShowLogin({ isShowLogin: true }))
              dispatch(
                changeUserInfo({
                  lalala: "11",
                }))
              // navigation.navigate('TestPagesEntry')
            }}
          />
        </View>
        <View style={styles.healthRecordCon}>
          <TouchableOpacity
            activeOpacity={touchableOpacityAlpha}
            onPress={() => {
              // navigateToPage('HealthFile', {}, true),
              //   haveHealthData
              //     ? nothingBuriedPoint('100000001', 'enter', [])
              //     : nothingBuriedPoint('100000011', 'enter', [{region: '0'}]);
            }}
            style={styles.healthRecordLeft}>
            <AnimatedCircularProgress
              size={parseInt(scale(100))}
              width={parseInt(scale(8))}
              backgroundWidth={parseInt(scale(8))}
              fill={isLogin?50:0}
              tintColor={healthColor}
              backgroundColor={`#EDEDED`}
              arcSweepAngle={360}
              rotation={0}
              duration={0}
              lineCap="round"
            >
              {() => (
                <View style={styles.circleProgressCon}>
                  <Image
                    source={require('../../assets/images/service/index/icon-flower.png')}
                    style={styles.flower}
                  />
                  <Text
                    style={[
                      styles.circleProgressText,
                      {
                        color:'#7DD685',
                      },
                    ]}>
                    ?
                  </Text>
                </View>
              )}
            </AnimatedCircularProgress>
            <View style={styles.healthGradeDesc}>
              {(
                <>
                  <Text style={styles.healthGradeDescText}>
                    录入数据后展示评分，
                  </Text>
                  <Text style={styles.healthGradeDescText}>
                    数据越完善，评分越准确
                  </Text>
                </>
              )}
            </View>
          </TouchableOpacity>
          {healthRecordRight(isLogin,currentBlood,(data)=>{
            if (isLogin){
              dispatch(
                changeSetUserInfo({
                  lalala: "1xxxxx1",
                })
              )
            }else {
              dispatch(
                changeUserInfo({
                  lalala: "11",
                }))
            }

            // alert(data)
          })}
          {/*<healthRecordRight*/}
          {/*  isLogin={isLogin}*/}
          {/*  currentBlood={currentBlood}*/}
          {/*  callback={()=>{*/}
          {/*  }}*/}
          {/*>*/}
          {/*</healthRecordRight>*/}

          {serviceHeaderView(props)}


        </View>
      </View>
    )


  }

  const healthRecordRightAction = (str) => {

    alert(str)
  }


  // const healthRecordRight = () => {
  //
  // }


  const ImageBackGround = () =>{

    if (this.imageBackGround){
      return this.imageBackGround
    }
    return (
      this.imageBackGround = <Image
        style={styles.containerBg}
        source={require('../../assets/images/service/index/bg.png')}
      />
    )
  }


  return (
    <View style={styles.container}>
      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        {ImageBackGround()}

      {/* 头部*/}
        {headerView()}
      </ScrollView>

    </View>

  );

}


export default React.memo(Service);
