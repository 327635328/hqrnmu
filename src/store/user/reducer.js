/*
 * @Author: leeym
 * @Date: 2021-05-15 11:03:15
 * @Last Modified by: Leeym
 * @Description: 用户相关数据 reducer
 */
import { produce } from 'immer';

import { types } from './actionTypes';

const defaultState = {
  isLogin: false,
  isVx: false,//是否是微信登录
  isShowLogin: false,//微信登录后触发登录model
  userInfo: {},
  token: '',
  accessToken: '',
  loginUserData: {},
  vxInfo: {},//存储微信授权的数据
};
export const userReducer = produce((state, action) => {
  switch (action.type) {
    case types.LOGININFO:
      // alert("LOGININFO")
      const { data } = action;

      state.isLogin = data.isLogin;

      if (data.isLogin) {
        // 存储用户相关数据
        state.loginUserData = data.loginUserData;
        state.token = `${data.loginUserData.token_type} ${data.loginUserData.access_token}`;
        state.accessToken = data.loginUserData.access_token;
      } else {
        // 未登录，清除所有存储的token等用户相关数据
        state.userInfo = {};
        state.token = '';
        state.accessToken = '';
        state.loginUserData = {};
      }
      break;
    case types.USERINFO:
      // alert(1)
      state.userInfo = action.data;

      state.isLogin = true;
      break;

    case types.SETUSERINFO:
      // alert(1)
      state.userInfo = action.data;

      state.isLogin = false;
      break;

    case types.ISVX:
      // alert(1)
      state.isVx = action.data;
      break;

    case types.ISSHOWLOGIN:
      // alert(1)
      state.isShowLogin = action.data;
      break;

    case types.VXINFO:
      // alert(1)
      state.vxInfo = action.data;
      break;
  }
}, defaultState);
