import React, {Component} from 'react';
import { View, Text, StyleSheet, FlatList, RefreshControl, ActivityIndicator, Button } from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {connect} from 'react-redux';
import action from '../../action/index'
import PopularItem from '../../common/PopularItem'
import { onLoadMorePopular } from "../../action/popular";
import Toast from 'react-native-easy-toast';
import SafeAreaView from "react-native/Libraries/Components/SafeAreaView/SafeAreaView";
import NavigationBar from "../../common/NavigationBar";
import FavoriteDao from "../../expand/dao/FavoriteDao";
import { FLAG_STORAGE } from "../../expand/dao/DataStore";
import FavoriteUtil from "../../util/FavoriteUtil";
import NavigationUtil from "../../navigator/NavigationUtil";
import EventBus from "react-native-event-bus";
import EventTypes from "../../util/EventTypes";


const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular);
const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const THEME_COLOR = "red"
const Tab = createMaterialTopTabNavigator();

type Props = {};
class PopularPage extends Component<Props> {


  constructor(props) {
    super(props);
    // this.tabNames = ['Java']
    // props.navigation.setOptions({
    //   headerShown: false,
    // })
    this.tabNames = ['Java','Android','iOS']
  }


  _genTabs() {
    const tabs = {};
    this.tabNames.forEach((item, index) => {
      tabs[`tab${index}`] = {
        screen: props => <PopularTabPage {...props} tabLabel={item}/>,
        navigationOptions: {
          title: item,
        },
      };
    });
    return tabs;
  }



  render()
  {

    const {theme} = this.props;
    return <SafeAreaView
      style = {{flex: 1}}
    >
      <NavigationContainer
        independent={true}
      >
        <Tab.Navigator

          screenOptions={(route) => ({
            tabBarStyle: styles.tabStyle,
            tabBarScrollEnabled: true, //是否支持 选项卡滚动，默认false
            tabBarActiveTintColor: 'white',
            tabBarContentContainerStyle: {
              backgroundColor: theme
            },
            tabBarIndicatorStyle: styles.indicatorStyle,
            tabBarLabelStyle: styles.labelStyle,//文字的样式
            lazy: true,
          })}
        >
          {
            Object.entries(this._genTabs()).map((item,index) => {
              return <Tab.Screen
                name={item[0]}
                component={item[1].screen}
                options={item[1].navigationOptions}
                key = {index}
              />;
            })
          }

        </Tab.Navigator>


      </NavigationContainer>
    </SafeAreaView>


  }
}

const mapPopularStateToProps = state => ({
  theme: state.theme.theme,
});

export default connect(mapPopularStateToProps)(PopularPage);

const pageSize = 10; //设为常量,防止修改
class PopularTab extends Component<Props> {

  constructor(props) {
    super(props);
    console.log(props.navigation);
    // const {navigation} = this.props;
    const {tabLabel} = this.props;
    this.storeName = tabLabel;
    // this.navigation = navigation;
    this.toastRef = React.createRef();
    this.isFavoriteChanged = false;
  }

  //组件完成装载时componentDidMount
  componentDidMount() {
    this.loadData();
    EventBus.getInstance().addListener(EventTypes.favorite_changed_popular, this.favoriteChangeListener = () => {
      this.isFavoriteChanged = true;
    });
    EventBus.getInstance().addListener(EventTypes.bottom_tab_select, this.bottomTabSelectListener = (data) => {
      if (data.to === 0 && this.isFavoriteChanged) {
        this.loadData(null, true);
      }
    });
  }

  componentWillUnmount() {
    EventBus.getInstance().removeListener(this.favoriteChangeListener);
    EventBus.getInstance().removeListener(this.bottomTabSelectListener);
  }

  loadData = (loadMore,refreshFavorite) =>{
    const {onRefreshPopular,onLoadMorePopular,onFlushPopularFavorite} = this.props;
    const store = this._store();
    const url = this.genFetchUrl(this.storeName);
    if (loadMore){
      onLoadMorePopular(this.storeName, ++store.pageIndex,pageSize ,store.items,favoriteDao,callback=>{
        this.toastRef.show('没有更多了')
      })
    }else if (refreshFavorite){
      alert("11")
      onFlushPopularFavorite(this.storeName,store.pageIndex,pageSize,store.items,favoriteDao)
    }

    else {
      onRefreshPopular(this.storeName, url ,pageSize,favoriteDao);
    }



  }

  /**
   * 获取与当前页面有关的数据
   * @returns {*}
   * @private
   */
  _store = () => {
    // console.log('_store')
    const {popular} = this.props;
    let store = popular[this.storeName];
    if (!store) {
      store = {
        items: [],
        isLoading: true,
        projectModels: [],//要显示的数据
        hideLoadingMore: true,//默认隐藏加载更多
      };
    }
    return store;
  }


  genFetchUrl(key) {
    return URL + key + QUERY_STR;
  }

  renderItem = (data) => {
    const item = data.item;
    return <PopularItem
      projectModel = {item}
      onSelect = {(callback)=>{
        const {navigation} = this.props;
        navigation.navigate("AboutPage");
        // this.props.navigation.getParent().navigation.navigate("AboutPage",{
        //
        // })
        // this.props.navigation.navigate("DetailPage",{
        //   projectModel: item,
        //   flag: FLAG_STORAGE.flag_popular,
        //   callback: (isFavorite)=>{
        //     alert("点击"+isFavorite)
        //   }
        // })

      }}
      onFavorite={(item, isFavorite) => {
        FavoriteUtil.onFavorite(favoriteDao,item,isFavorite,FLAG_STORAGE.flag_popular)

      }}

    >

    </PopularItem>
  }

  genIndicator() {
    return this._store().hideLoadingMore ? null :
      <View style={styles.indicatorContainer}>
        <ActivityIndicator
          style={styles.indicator}
        />
        <Text>正在加载更多</Text>
      </View>;
  }

  render() {
    // const {popular} = this.props;
    let store = this._store();
    return <View style={styles.container}>
      <FlatList
        data={store.projectModels}
        renderItem={data => this.renderItem(data)}
        keyExtractor={item => '' + item.item.id}
        scrollEventThrottle={50}
        removeClippedSubviews={false}
        ListEmptyComponent={()=>{
          return <View style={{flex: 1,backgroundColor: "yellow"}}></View>
        }}
        refreshControl={
          <RefreshControl
            title={'Loading'}
            titleColor={THEME_COLOR}
            colors={[THEME_COLOR]}
            refreshing={store.isLoading}
            onRefresh={() => this.loadData(false)}
            tintColor={THEME_COLOR}
            progressViewOffset={30}
          />
        }
        ListFooterComponent={() => this.genIndicator()}
        onEndReached={() => {
          console.log('---onEndReached----');
          setTimeout(()=>{
            if (this.canLoadMore){
              this.loadData(true)
              this.canLoadMore = false
            }
          },100);

        }}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {
          this.canLoadMore = true; //fix 初始化时页调用onEndReached的问题
          console.log('---onMomentumScrollBegin-----');
        }}
      />
      <Toast
        ref={ref => this.toastRef = ref}
        position={'center'}
      ></Toast>
    </View>;
  }

}

const mapStateToProps = state => ({
  popular: state.popular,
});
const mapDispatchToProps = dispatch => ({
  //将 dispatch(onRefreshPopular(storeName, url))绑定到props
  onRefreshPopular: (storeName, url, pageSize,favoriteDao) => dispatch(action.onRefreshPopular(storeName, url,pageSize,favoriteDao)),
  onLoadMorePopular: (storeName, pageIndex, pageSize, items, favoriteDao,callBack) => dispatch(action.onLoadMorePopular(storeName, pageIndex, pageSize, items, favoriteDao,callBack)),
  onFlushPopularFavorite: (storeName, pageIndex, pageSize, items, favoriteDao) => dispatch(action.onFlushPopularFavorite(storeName, pageIndex, pageSize, items, favoriteDao)),
});

const PopularTabPage = connect(mapStateToProps, mapDispatchToProps)(PopularTab);


const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});
