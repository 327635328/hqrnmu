import React, {Component} from 'react';
import { View, Text, StyleSheet, Button, TextInput, TouchableOpacity } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";


const KEY = "save_key"


type Props = {};
export default class AsyncStoragePage extends Component<Props> {
  constructor(props) {
    super(props);
    console.log('喜欢的')
    this.state = {
      showText : '',

    }
  }



  render() {
    return <View style={styles.container}>
      <TextInput
        style={styles.input}
        onChangeText={text => {
          this.value = text;
        }}
      />
      <View style = {styles.input_container}>

        <Button
          title={"存储"}
          onPress={()=>{
            this.doSave();
          }}
        />
        <Button
          title={"删除"}
          onPress={()=>{
            this.doRemove();
          }}
        />
        <Button
          title={"获取"}
          onPress={()=>{
            this.getData();
          }}
        />
      </View>

      <Text>
        {this.state.showText}
      </Text>
    </View>;
  }

  async doSave() {
    AsyncStorage.setItem(KEY,this.value,error => {
      error && console.log(error.toString());
    })
  }



  async doRemove() {
    AsyncStorage.removeItem(KEY,error => {
      error && console.log(error.toString());
    })
  }

  async getData() {
    AsyncStorage.getItem(KEY,(error,value)=>{
      this.setState({
        showText: value
      });
      // console.log(value);
      error && console.log(error.toString());
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 30,
    // flex:1,
    borderColor: "black",
    borderWidth: 1,
    marginRight: 10,
  },
  input_container: {
    flexDirection: "row",
    justifyContent: "center"
  }
});

