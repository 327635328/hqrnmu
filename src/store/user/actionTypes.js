/*
 * @Author: leeym
 * @Date: 2021-05-15 10:46:57
 * @Last Modified by: Leeym
 * @Description: 用户相关数据 active-types
 */
export const types = {
  LOGININFO: 'LOGIN_INFO',
  USERINFO: 'USER_INFO',
  SETUSERINFO:'SETUSERINFO',
  ISVX:'ISVX',
  ISSHOWLOGIN:'ISSHOWLOGIN',
  VXINFO:'VXINFO'

};
