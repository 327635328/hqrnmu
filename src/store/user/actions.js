/*
 * @Author: leeym
 * @Date: 2021-05-15 10:52:40
 * @Last Modified by: Leeym
 * @Description: 用户相关数据 action
 */
import {types} from './actionTypes';

export const changeLoginInfo = data => dispatch =>
  dispatch({
    type: types.LOGININFO,
    data,
  });

export const changeUserInfo = data => dispatch =>
  dispatch({
    type: types.USERINFO,
    data,
  });

export const changeSetUserInfo = data => dispatch =>
  dispatch({
    type: types.SETUSERINFO,
    data,
  });


export const changeIsVx = data => dispatch =>
  dispatch({
    type: types.ISVX,
    data,
  });

export const changeIsShowLogin = data => dispatch =>
  dispatch({
    type: types.ISSHOWLOGIN,
    data,
  });

export const changeVxInfo = data => dispatch =>
  dispatch({
    type: types.VXINFO,
    data,
  });
