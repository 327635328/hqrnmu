import Types from '../../action/types'


const defaultState = {};

/**
 * favorite:{
 *     popular:{
 *         projectModels:[],
 *         isLoading:false
 *     },
 *     trending:{
 *         projectModels:[],
 *         isLoading:false
 *     }
 * }
 * 0.state树，横向扩展
 * 1.如何动态的设置store，和动态获取store(难点：store key不固定)；
 * @param state
 * @param action
 * @returns {{theme: (onAction|*|string)}}
 */
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.FAVORITE_LOAD_DATA://获取数据
      return {
        ...state,//...延展操作符
        [action.storeName]: {//这里为了从action中取出storeName并作为{}中的key使用所以需要借助[]，否则会js语法检查不通过
          ...state[action.storeName],//这里是为了解构state中action.storeName对应的属性，所以需要用到[]
          isLoading: false,
        },
      };
    case Types.FAVORITE_LOAD_SUCCESS://下拉获取数据成功
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: false,
          projectModels:action.projectModels,
        },
      };
    case Types.FAVORITE_LOAD_FAIL://下拉刷新失败
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: false,
        },
      };

    default:
      return state;
  }

}
