import React, { useEffect, useState } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { styles } from "./serviceStyles";
import { healthDataColors } from "./serviceManager";

const touchableOpacityAlpha = 0.6;
export const healthRecordRight  = (isLogin,currentBlood,callBack) =>{

  // const [isLogin,setIsLogin] = useState(login)


  // useEffect(() => {
  //
  //   // if (isLogin && global.token) {
  //   //   if (!haveData) getHealthData();
  //   // } else {
  //   //   haveData = false;
  //   // }
  // }, [isLogin]);


  const healthRecordRightAction = (str) => {
    callBack(str)

  }

  let arr = ['血压','心率','体重','睡眠']
  return (
    <View style={styles.healthRecordRight}>
      {
        arr.map((item,index)=>{
          return <TouchableOpacity
            key={index}
            activeOpacity={touchableOpacityAlpha}
            style={styles.healthRecordItem}
            onPress={() => healthRecordRightAction(item)}
          >
            {!isLogin || !currentBlood.inputToday ? (
              <>
                <View style={styles.flexRow}>
                  <Text style={styles.recordTitle}>{arr[index]}</Text>
                  <Text style={styles.recordTips}>{isLogin?"111111":"动动手指，记一下"} </Text>
                </View>
                <Image
                  style={styles.recordArrow}
                  source={require('../assets/images/service/index/icon-arrow-right-01.png')}
                />
              </>
            ) : (
              <>
                <View style={styles.flexRow}>
                  <Text style={styles.recordTitle}>血压</Text>
                  <Text
                    style={[
                      styles.recordTitle,
                      {
                        color:
                          healthDataColors.currentBlood[
                          currentBlood.describe || '正常'
                            ],
                      },
                    ]}>
                    {currentBlood.sbp}/{currentBlood.dpd}
                  </Text>
                  <Text style={styles.recordTips}>mmHg</Text>
                </View>
                <View
                  style={[
                    styles.recordDescBox,
                    {
                      backgroundColor:
                        healthDataColors.currentBlood[
                        currentBlood.describe || '正常'
                          ],
                    },
                  ]}>
                  <Text style={styles.recordDesc}>
                    {currentBlood.describe}
                  </Text>
                </View>
              </>
            )}
          </TouchableOpacity>
        })
      }
    </View>
  )
}
