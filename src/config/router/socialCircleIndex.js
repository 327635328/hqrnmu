import React, {useEffect, useLayoutEffect} from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";



const SocialCircle = props => {
  const navigation = useNavigation();



  return (
    <View style={styles.container}>
      <Button
        title={"点击"}
        onPress={()=>{
          navigation.navigate("AboutPage")
        }}
      >

      </Button>
    </View>

  );

}


const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "green",
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});

export default SocialCircle;
