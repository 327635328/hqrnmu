import React,  { useState, useEffect, useRef } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from "react-native";
import DataStore from '../../expand/dao/DataStore'
import AsyncStorage from "@react-native-async-storage/async-storage";
const KEY = "save_key"
import {useNavigation} from '@react-navigation/native';

type Props = {};
const MyPage = props =>{
  const navigation = useNavigation();
  const [showText, setShowText] = useState(); //选中的注销原因内容
  const aaa = () =>{
    navigation.getParent().navigate("AboutPage");
  }
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     showText : '',
  //   }
  //   this.navigation = useNavigation();
  //   this.dataStore = new DataStore();
  // }


    return (
      <View style={styles.container}>
      <TextInput
        style={styles.input}
        onChangeText={text => {
          this.value = text;
        }}
      />
      <View style = {styles.input_container}>

        <Button
          title={"存储"}
          onPress={()=>{
            aaa();
            // this.doSave();
          }}
        />
        <Button
          title={"删除"}
          onPress={()=>{
            this.doRemove();
          }}
        />
        <Button
          title={"获取"}
          onPress={()=>{
            this.getData();
          }}
        />
      </View>

      <Text>
        {/*{this.state.showText}*/}
      </Text>
    </View>
    )



  // async doSave() {
  //
  //
  //
  //   AsyncStorage.setItem(KEY,this.value,error => {
  //     error && console.log(error.toString());
  //   })
  // }
  //
  //
  //
  // async doRemove() {
  //   AsyncStorage.removeItem(KEY,error => {
  //     error && console.log(error.toString());
  //   })
  // }
  //
  // async getData() {
  //   AsyncStorage.getItem(KEY,(error,value)=>{
  //     this.setState({
  //       showText: value
  //     });
  //     console.log(value);
  //     error && console.log(error.toString());
  //   })
  // }
  //
  // loadData(){
  //   let url = `https://api.github.com/search/repositories?q=${this.value}`;
  //   this.dataStore.fetchData(url)
  //     .then(data=>{
  //       console.log('数据返回')
  //       // console.log(data)
  //       let showData = `初次数据加载时间：${new Date(data.timestamp)}\n${JSON.stringify(data.data)}`;
  //       this.setState({
  //         showText: showData,
  //       })
  //     })
  //     .catch(error => {
  //       error&&alert(error.toString())
  //     })
  // }

}



const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    height: 30,
    // flex:1,
    borderColor: "black",
    borderWidth: 1,
    marginRight: 10,
  },
  input_container: {
    flexDirection: "row",
    justifyContent: "center"
  }
});

export default MyPage;
