import {combineReducers} from 'redux';

import {userReducer} from './user/reducer';
// import {globalReducer} from './global/reducer';
// import {addressReducer} from './address/reducer';
// import {mallReducer} from './mall/reducer';
// import {bluetoothReducer} from './bluetooth/reducer';
// import {discoverProducer} from './discover/reducer';
// import {serviceProducer} from './service/reducer';

export default combineReducers({
  user: userReducer,
  // global: globalReducer,
  // address: addressReducer,
  // mall: mallReducer,
  // bluetooth: bluetoothReducer,
  // discover: discoverProducer,
  // service: serviceProducer
});
