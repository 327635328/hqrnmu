import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  RefreshControl,
} from "react-native";
import NavigationBar from "../../common/NavigationBar";
import NavigationUtil from "../../navigator/NavigationUtil";
import Feather from "react-native-vector-icons/Feather";
import {connect} from 'react-redux';
import action from '../../action/index'
import PopularItem from "../../common/PopularItem";
import { FLAG_STORAGE } from "../../expand/dao/DataStore";
import FavoriteUtil from "../../util/FavoriteUtil";
import Toast from "react-native-easy-toast";
import { NavigationContainer } from "@react-navigation/native";
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import FavoriteDao from "../../expand/dao/FavoriteDao";
import EventBus from "react-native-event-bus";
import EventTypes from "../../util/EventTypes";




const THEME_COLOR = "red"
const Tab = createMaterialTopTabNavigator();
const TABS = () => ({
  'Popular': {
    screen: props => <FavoriteTabPage {...props} flag={FLAG_STORAGE.flag_popular}/>,//初始化Component时携带默认参数 @https://github.com/react-navigation/react-navigation/issues/2392
    navigationOptions: {
      title: '最热',
    },
  },
  'Trending': {
    screen: props => <FavoriteTabPage {...props} flag={FLAG_STORAGE.flag_trending}/>,//初始化Component时携带默认参数 @https://github.com/react-navigation/react-navigation/issues/2392
    navigationOptions: {
      title: '趋势',
    },
  },
});

type Props = {};
class FavoritePage extends Component<Props> {
  constructor(props) {
    super(props);
    console.log('喜欢的')
    props.navigation.setOptions({
      headerShown: false,
    })
    this.tabNames = ['最热','趋势']
  }







  rightBtn(){
    return <TouchableOpacity
      style={{paddingRight: 8}}
      onPress={()=> {
        NavigationUtil.navigation.navigate("AsyncStoragePage")
      }}
    >
      <Feather
        name={'award'}
        size={26}
        style={{color: 'white'}}/>
    </TouchableOpacity>
  }



  render() {
    const {theme} = this.props
    let navBar = <NavigationBar
      title={"收藏"}
      rightButton={this.rightBtn()}
    >
    </NavigationBar>

    return <View style={styles.container}>
      {navBar}
      <NavigationContainer
        independent={true}
      >
        <Tab.Navigator

          screenOptions={(route) => ({
            tabBarStyle: styles.tabStyle,
            tabBarScrollEnabled: false, //是否支持 选项卡滚动，默认false
            tabBarActiveTintColor: 'white',
            tabBarContentContainerStyle: {
              backgroundColor: theme
            },
            tabBarIndicatorStyle: styles.indicatorStyle,
            tabBarLabelStyle: styles.labelStyle,//文字的样式
            lazy: true,
          })}
        >


          {
            Object.entries(TABS()).map((item,index) => {
              return <Tab.Screen
                name={item[0]}
                component={item[1].screen}
                options={item[1].navigationOptions}
                key = {index}
              />;
            })
          }

        </Tab.Navigator>


      </NavigationContainer>
    </View>;
  }

}


const mapFavoriteStateProps = state => ({
  theme: state.theme.theme
})

export default connect(mapFavoriteStateProps)(FavoritePage);


class FavoriteTab extends Component<Props> {

  constructor(props) {
    super(props);
    const {flag} = this.props;
    this.storeName = flag;
    this.favoriteDao = new FavoriteDao(flag)
    this.toastRef = React.createRef();
  }

  //组件完成装载时componentDidMount
  componentDidMount() {
    this.loadData(true);
    EventBus.getInstance().addListener(EventTypes.bottom_tab_select,this.listener = data =>{
      if (data.to == 2){
        this.loadData(false);
      }
    })
  }

  componentWillUnmount() {
    EventBus.getInstance().removeListener(this.listener);
  }


  loadData = (isShowLoading) =>{
    const {onLoadFavoriteData} = this.props;
    onLoadFavoriteData(this.storeName,isShowLoading)

  }

  /**
   * 获取与当前页面有关的数据
   * @returns {*}
   * @private
   */
  _store = () => {
    // console.log('_store')
    const {favorite} = this.props;
    let store = favorite[this.storeName];
    if (!store) {
      store = {
        isLoading: true,
        projectModels: [],//要显示的数据
      };
    }
    return store;
  }

  onFavorite(item, isFavorite){
    FavoriteUtil.onFavorite(this.favoriteDao,item,isFavorite,this.storeName)
    if (this.storeName === FLAG_STORAGE.flag_popular) {
      EventBus.getInstance().fireEvent(EventTypes.favorite_changed_popular);
    } else {
      EventBus.getInstance().fireEvent(EventTypes.favoriteChanged_trending);
    }
  }

  renderItem(data) {
    const item = data.item;
    return <PopularItem
      projectModel = {item}
      onSelect = {(callback)=>{
        NavigationUtil.goPage("DetailPage",{
          projectModel: item,
          flag: this.storeName,
          callback,
        })
      }}
      onFavorite={(item, isFavorite) => this.onFavorite()}

    >

    </PopularItem>
  }



  render() {
    // const {popular} = this.props;
    let store = this._store();
    return <View style={styles.container}>
      <FlatList
        data={store.projectModels}
        renderItem={data => this.renderItem(data)}
        keyExtractor={item => '' + item.item.id}
        scrollEventThrottle={50}
        removeClippedSubviews={false}
        ListEmptyComponent={()=>{
          return <View style={{flex: 1,backgroundColor: "yellow"}}></View>
        }}
        refreshControl={
          <RefreshControl
            title={'Loading'}
            titleColor={THEME_COLOR}
            colors={[THEME_COLOR]}
            refreshing={store.isLoading}
            onRefresh={() => this.loadData(true)}
            tintColor={THEME_COLOR}
            progressViewOffset={30}
          />
        }

      />
      <Toast
        ref={ref => this.toastRef = ref}
        position={'center'}
      ></Toast>
    </View>;
  }

}

const mapStateToProps = state => ({
  favorite: state.favorite,
});
const mapDispatchToProps = dispatch => ({
  onLoadFavoriteData: (storeName, isShowLoading) => dispatch(action.onLoadFavoriteData(storeName, isShowLoading)),
});

const FavoriteTabPage = connect(mapStateToProps, mapDispatchToProps)(FavoriteTab);


const styles = StyleSheet.create({
  container:{
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});
