import React from "react";
import { StyleSheet } from "react-native";
import { scale } from "react-native-size-matters";
import { getStatusBarHeight } from "react-native-status-bar-height";


export const serviceStyles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});


export const styles = StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
  },
  containerBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: scale(555),
  },
  noDataTipsCon: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  noDataTips: {
    fontSize: scale(12),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    lineHeight: scale(12),
    color: '#808080',
  },
  /* 顶部个人健康信息模块 Style Begin */
  header: {
    // height: 200,
    backgroundColor:"red",
    // width: scale(350),
    paddingHorizontal: scale(14),
  },
  titleCon: {
    backgroundColor: "green",
    paddingVertical: scale(12),
  },
  title: {
    marginTop:getStatusBarHeight(),
    fontSize: scale(18),
    fontFamily: 'PingFang SC',
    fontWeight: '500',
    color: '#FFFFFF',
  },
  healthRecordCon: {
    paddingHorizontal: scale(14),
    paddingVertical: scale(26),
    backgroundColor: '#fff',
    borderRadius: scale(14),
    elevation: scale(6),
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowRadius: scale(14),
    shadowOpacity: scale(0.04),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom:10,
  },
  healthRecordLeft: {
    width: 118,
    alignItems: 'center',
  },
  circleProgressCon: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    paddingVertical: scale(14),
  },
  flower: {
    width: scale(16.8),
    height: scale(20),
    // marginBottom: scale(4),
  },
  circleProgressText: {
    fontSize: scale(36),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#2AD1B6',
    lineHeight: scale(38),
  },
  healthGradeDesc: {
    marginTop: scale(10),
  },
  healthGradeDescText: {
    fontSize: scale(9),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#999999',
    lineHeight: scale(14),
  },
  healthRecordRight: {
    flex: 1,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: '#EEEEEE',
    marginLeft: scale(16),
  },
  healthRecordItem: {
    height: scale(42),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d3d3d3',
  },
  healthRecordItemCon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    height: '100%',
  },
  recordTitle: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '500',
    color: '#333333',
    marginRight: scale(6),
  },
  recordTips: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#c0c0c0',
  },
  recordArrow: {
    width: scale(7),
    height: scale(13),
  },
  recordDescBox: {
    minWidth: scale(34),
    paddingHorizontal: scale(5),
    height: scale(15),
    borderRadius: scale(8),
    backgroundColor: '#8ED9B0',
    justifyContent: 'center',
    alignItems: 'center',
  },
  recordDesc: {
    fontSize: scale(8),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#fff',
    textAlign: 'center',
  },
  /* 顶部个人健康信息模块 Style End */

  /* 功能入口模块 Style Begin */
  moduleEntries: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(14),
    paddingVertical: scale(7),
  },
  moduleEntryItem: {
    flex: 1,
    borderRadius: scale(14),
    elevation: scale(6),
    shadowColor: 'rgba(0, 0, 0, 1)',
    shadowRadius: scale(14),
    shadowOpacity: scale(0.04),
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: scale(20),
    paddingHorizontal: scale(16),
  },
  moduleEntryImg: {
    width: scale(39),
    height: scale(38),
  },
  moduleEntryDescCon: {
    justifyContent: 'space-around',
  },
  moduleEntryTitle: {
    fontSize: scale(14),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#000',
    lineHeight: scale(16),
  },
  moduleEntryDesc: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#999',
    lineHeight: scale(16),
  },
  /* 功能入口模块 Style End */

  section: {
    marginBottom: scale(8),
    backgroundColor: '#fff',
    paddingHorizontal: scale(14),
    paddingTop: scale(8),
    paddingBottom: scale(28),
  },
  sectionTitleCon: {
    paddingVertical: scale(12),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  sectionTitle: {
    fontSize: scale(17),
    fontFamily: 'PingFang SC',
    fontWeight: '500',
    color: '#000',
  },
  sectionMore: {
    fontSize: scale(10),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#999',
    marginRight: scale(5),
  },
  sectionMoreIcon: {
    width: scale(6),
    height: scale(10),
  },

  /* 饮食推荐等 Style Begin */
  recommendCon: {
    paddingHorizontal: 0,
    paddingTop: scale(10),
    paddingBottom: scale(15),
  },
  recommendTabs: {
    height: scale(67),
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  recommendTabsBg: {
    width: '100%',
    height: scale(67),
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  energyInfo: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: scale(14),
    paddingBottom: scale(6),
  },
  intakeTip: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '500',
    color: '#999',
  },
  energyVal: {
    fontSize: scale(17),
    fontWeight: 'bold',
    marginRight: scale(4),
    color: '#000',
  },
  recommendBtn: {
    position: 'relative',
    width: scale(130),
    height: scale(67),
    paddingRight: scale(36),
    paddingTop: scale(6),
    paddingBottom: scale(9),
  },
  recommendBtnBg: {
    width: scale(130),
    height: scale(67),
    position: 'absolute',
    left: 0,
    top: 0,
  },
  recommendBtnText: {
    fontSize: scale(14),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#fff',
  },
  moreRecommendBtnCon: {
    alignItems: 'center',
  },
  moreRecommendBtnBg: {
    width: scale(190),
    height: scale(62),
    paddingTop: scale(5),
    paddingBottom: scale(16),
  },
  dietRecommendCon: {
    width: scale(286),
    backgroundColor: '#fff',
    marginLeft: scale(14),
    // marginTop: scale(2),
    marginBottom: scale(20),
    paddingTop: scale(15),
    paddingBottom: scale(20),
    paddingLeft: scale(20),
    paddingRight: scale(15),
    borderRadius: scale(15),
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#d3d3d3',
  },
  dietType: {
    lineHeight: scale(35),
    fontSize: scale(15),
    fontFamily: 'PingFang SC',
    fontWeight: '500',
    color: '#000',
  },
  dietList: {},
  dietItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: scale(15),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d3d3d3',
  },
  foodImg: {
    width: scale(45),
    height: scale(45),
    borderRadius: scale(8),
  },
  foodName: {
    fontSize: scale(13),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#666',
    marginLeft: scale(10),
    width: scale(90),
  },
  calories: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#65CEB7',
  },
  foodCount: {
    fontSize: scale(10),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#C9C9C9',
  },
  sportRecommendCon: {
    paddingLeft: scale(20),
    paddingRight: scale(20),
    paddingBottom: scale(26),
    paddingTop: 0,
  },
  sportItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: scale(12),
    paddingBottom: scale(6),
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#d3d3d3',
  },
  sportIcon: {
    width: scale(40),
    height: scale(40),
    marginRight: scale(3),
  },
  sportTitle: {
    fontSize: scale(13),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#666666',
    lineHeight: scale(21),
  },
  courseRecommendCon: {
    paddingLeft: scale(16),
    paddingRight: scale(16),
    paddingBottom: scale(20),
    paddingTop: scale(12),
  },
  courseItem: {
    marginBottom: scale(10),
    alignItems: 'center',
  },
  courseBg: {
    width: scale(254),
    height: scale(72),
    borderRadius: scale(8),
    overflow: 'hidden',
    position: 'relative',
  },
  courseDetailsCon: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    paddingHorizontal: scale(14),
    borderRadius: scale(8),
    overflow: 'hidden',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, .4)',
  },
  courseTitle: {
    fontSize: scale(14),
    fontFamily: 'PingFang SC',
    fontWeight: 'bold',
    color: '#fff',
  },
  /* 饮食推荐等 Style End */

  /* 热门圈子 Style Begin */
  circleList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  circleItem: {
    width: scale(68),
    alignItems: 'center',
  },
  circleImg: {
    width: scale(68),
    height: scale(68),
    borderRadius: scale(34),
  },
  circleName: {
    fontSize: scale(12),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#666666',
    marginTop: scale(4),
  },
  /* 热门圈子 Style End */

  /* 精选好物 Style Begin */
  goodsList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  goodsItem: {
    width: scale(156),
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#eee',
    borderRadius: scale(8),
  },
  goodsImg: {
    width: scale(156),
    height: scale(132),
    borderTopLeftRadius: scale(8),
    borderTopRightRadius: scale(8),
  },
  goodsDetails: {
    padding: scale(10),
    paddingBottom: scale(14),
  },
  goodsTitle: {
    fontSize: scale(15),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#000',
    lineHeight: scale(21),
  },
  goodsDesc: {
    fontSize: scale(12),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#999',
    height: scale(30),
  },
  goodsPrice: {
    color: '#FF6C00',
    fontSize: scale(14),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
  },
  /* 精选好物 Style End */

  /* 健康资讯 Style Begin */
  articleList: {},
  articleItem: {
    paddingVertical: scale(20),
    borderBottomColor: '#eee',
  },
  articleTitle: {
    fontSize: scale(13),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#333',
    lineHeight: scale(18),
  },
  articleImgCon: {
    marginVertical: scale(10),
    borderRadius: scale(8),
    overflow: 'hidden',
  },
  articleImg: {
    width: '100%',
    height: scale(180),
    marginVertical: scale(10),
    borderRadius: scale(8),
  },
  articleFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  articleAuthor: {
    fontSize: scale(11),
    fontFamily: 'PingFang SC',
    fontWeight: '400',
    color: '#999',
  },
  operationIcon: {
    width: scale(20),
    height: scale(20),
    marginRight: scale(5),
  },
  headerTouch:{
    backgroundColor:"yellow",
    width: scale(40),
    height: '100%',
    position: 'absolute',
    top: getStatusBarHeight(),
    right: scale(-14),
  }
  /* 健康资讯 Style End */
});
