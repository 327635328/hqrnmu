import React, {Component} from 'react';
import { View,Text,StyleSheet } from "react-native";
import DynamicTabNavigator from "../navigator/DynamicTabNavigator";
import NavigationUtil from "../navigator/NavigationUtil";

type Props = {};
export default class HomePage extends Component<Props> {
  constructor(props) {
    super(props);
  }


  render() {
    NavigationUtil.navigation = this.props.navigation;
    return <DynamicTabNavigator/>
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  }
});
