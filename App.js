/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from "react";
import AppNavigators from './js/navigator/AppNavigators'
import {Provider} from 'react-redux';
import store from './js/store'


type Props = {};
export default class App extends Component<Props> {
  render() {
    const App = AppNavigators();
    /**
     * 将store传递给App框架
     */
    return <Provider store={store}>
      {App}
    </Provider>;
  }
}

