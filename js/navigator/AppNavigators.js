import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import WelcomePage from "../page/WelcomePage";
import HomePage from "../page/HomePage";
import DetailPage from "../page/DetailPage";
import WebViewPage from "../page/WebViewPage";
import AboutPage from "../page/AboutPage";
import AboutMePage from "../page/AboutMePage";
import CustomKeyPage from "../page/CustomKeyPage";
import SortKeyPage from "../page/SortKeyPage";
import SearchPage from "../page/SearchPage";
import CodePushPage from "../page/CodePushPage";
import AsyncStoragePage from "../page/AsyncStoragePage";
import { HomeComponent } from "../../src/config/router/home";
import Service from "../../src/config/router/serviceIndex";
// import Service from "../config/router/serviceIndex";

const Stack = createStackNavigator();

export default function AppNavigators() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Home'
      >
        {/*<Stack.Screen name="WelcomePage" component={WelcomePage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="HomePage" component={HomePage}*/}
        {/*              options={{headerShown: false, animationEnabled: false}}/>*/}
        {/*<Stack.Screen name="DetailPage" component={DetailPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="WebViewPage" component={WebViewPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        <Stack.Screen name="AboutPage" component={AboutPage}
                      options={{headerShown: true}}/>
        {/*<Stack.Screen name="AboutMePage" component={AboutMePage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="CustomKeyPage" component={CustomKeyPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="SortKeyPage" component={SortKeyPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="SearchPage" component={SearchPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="CodePushPage" component={CodePushPage}*/}
        {/*              options={{headerShown: false}}/>*/}
        {/*<Stack.Screen name="AsyncStoragePage" component={AsyncStoragePage}*/}
        {/*              options={{headerShown: true}}/>*/}
        <Stack.Screen
          name="Home"
          component={HomeComponent}
          options={{headerShown: false}}
        />
        {/*<Stack.Screen*/}
        {/*  name="Service"*/}
        {/*  component={Service}*/}
        {/*  options={{*/}
        {/*    headerShown: false,*/}
        {/*    headerTitle: '服务',*/}
        {/*  }}*/}
        {/*/>*/}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
