import React from "react";

export const healthDataColors = {
  currentBlood: {
    低血压: '#A1D3E8',
    正常: '#69E6A6',
    临界值: '#66E877',
    轻度: '#EDC31B',
    中度: '#FF831B',
    重度: '#FF5D40',
  },
  heartRate: {
    心动过缓: '#A1D3E8',
    正常: '#69E6A6',
    心动过速: '#FF5D40',
  },
  weightInfo: {
    过轻: '#A1D3E8',
    正常: '#69E6A6',
    过重: '#EDC31B',
    肥胖: '#FF831B',
    非常肥胖: '#FF5D40',
  },
  sleepData: {
    很差: '#A1D3E8',
    一般: '#7CDEC8',
    尚可: '#69E6A6',
    很好: '#66E877',
    睡过头了: '#FF5D40',
  },
};

export const healthColorList = ['#67C6D5', '#00C395', '#2EC300', '#FE8441', '#FF0000'];


export const judgmentData = [
  ['4:00:00', '10:00:00'],
  ['10:00:01', '15:00:00'],
  ['15:00:01', '23:59:59'],
  ['00:00:00', '3:59:59'],
];
