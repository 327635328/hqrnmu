import React, { Component } from "react";
import {Modal, Text, TouchableOpacity, StyleSheet, View, Platform, DeviceInfo} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import {getStatusBarHeight} from 'react-native-status-bar-height'
import TimeSpan from "../model/TimeSpan";

const NAV_BAR_HEIGHT_IOS = 44;//导航栏在iOS中的高度
const NAV_BAR_HEIGHT_ANDROID = 50;//导航栏在Android中的高度

const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? NAV_BAR_HEIGHT_IOS  : NAV_BAR_HEIGHT_ANDROID;
export const TimeSpans = [new TimeSpan('今 天', 'since=daily'),
  new TimeSpan('本 周', 'since=weekly'), new TimeSpan('本 月', 'since=monthly')];
export default class TrendingDialog extends Component
{
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    }
  }

  show() {
    this.setState({
      visible: true,
    });
  }

  dismiss() {
    this.setState({
      visible: false,
    });
  }

  render() {
    const {onClose, onSelect} = this.props;
    return (<Modal
      //transparent 透明
      transparent={true}
      visible={this.state.visible}
      onRequestClose={() => onClose}
    >
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.dismiss()}
      >
        <MaterialIcons
          name={'arrow-drop-up'}
          size={36}
          style={styles.arrow}
        />
        <View style={styles.content}>
          {TimeSpans.map((result, i, arr) => {
            return <TouchableOpacity
              key={i}
              onPress={() => onSelect(arr[i])}
              underlayColor='transparent'>
              <View style={styles.text_container}>
                <Text
                  style={styles.text}
                >{arr[i].showText}</Text>
              </View>
              {
                i !== TimeSpans.length - 1 ? <View
                  style={styles.line}
                /> : null
              }
            </TouchableOpacity>;
          })}
        </View>
      </TouchableOpacity>
    </Modal>);
  }


}



const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    flex: 1,
    alignItems: 'center',
    paddingTop: getStatusBarHeight(),
  },
  arrow: {
    marginTop: 20,
    color: 'white',
    padding: 0,
    margin: -15,
  },
  content: {
    backgroundColor: 'white',
    borderRadius: 3,
    paddingTop: 3,
    paddingBottom: 3,
    marginRight: 3,
  },
  text_container: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontSize: 16,
    color: 'black',
    fontWeight: '400',
    padding: 8,
    paddingLeft: 26,
    paddingRight: 26,
  },
  line: {
    height: 0.3,
    backgroundColor: 'darkgray',
  },
});
