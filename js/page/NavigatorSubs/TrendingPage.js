import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Modal,
  TouchableOpacity,
} from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {connect} from 'react-redux';
import action from '../../action/index'
import PopularItem from '../../common/PopularItem'
import { onLoadMoreTrending } from "../../action/trending";
import NavigationUtil from "../../navigator/NavigationUtil";
import NavigationBar from "../../common/NavigationBar";
import Ionicons from "react-native-vector-icons/Ionicons";
import Feather from "react-native-vector-icons/Feather";
import Toast from 'react-native-easy-toast';
import SafeAreaView from "react-native/Libraries/Components/SafeAreaView/SafeAreaView";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import TrendingDialog, { TimeSpans } from "../../common/TrendingDialog";
import TrendingItem from "../../common/TrendingItem";
import FavoriteDao from "../../expand/dao/FavoriteDao";
import { FLAG_STORAGE } from "../../expand/dao/DataStore";
import FavoriteUtil from "../../util/FavoriteUtil";


const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_trending)
const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const THEME_COLOR = "red"
const Tab = createMaterialTopTabNavigator();

type Props = {};
class TrendingPage extends Component<Props> {

  constructor(props) {
    super(props);
    this.state = {
      timeSpan:TimeSpans[0],
    };
    props.navigation.setOptions({
      headerShown: false,
    })
    this.tabNames = ['React','React Native','PHP']
  }


  _genTabs() {
    const tabs = {};
    this.tabNames.forEach((item, index) => {
      tabs[`tab${index}`] = {
        screen: props => <TrendingTabPage {...props} tabLabel={item}/>,
        navigationOptions: {
          title: item,
        },
      };
    });
    return tabs;
  }

  renderTitleView() {
    return <View>
      <TouchableOpacity
        underlayColor='transparent'
        onPress={() => this.dialog.show()}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={{
            fontSize: 18,
            color: '#FFFFFF',
            fontWeight: '400',
          }}>趋势 {this.state.timeSpan.showText}</Text>
          <MaterialIcons
            name={'arrow-drop-down'}
            size={22}
            style={{color: 'white'}}
          />
        </View>
      </TouchableOpacity>
    </View>;
  }

  onSelectTimeSpan(tab) {
    this.dialog.dismiss();
    this.setState({
      timeSpan: tab,
    });
    // DeviceEventEmitter.emit(EVENT_TYPE_TIME_SPAN_CHANGE, tab);
  }

  renderTrendingDialog(){
    return <TrendingDialog
      ref={dialog => this.dialog = dialog}
      onSelect={tab => this.onSelectTimeSpan(tab)}
    />;
  }

  render() {
    let navigationBar = <NavigationBar
      // title={'趋势'}
      titleView={this.renderTitleView()}
      // statusBar={statusBar}
      style={{backgroundColor: "red",}}
      rightButton={this.right()}
      // leftButton={this.left()}
    >
    </NavigationBar>
    const {theme} = this.props;
    return <View style={styles.container}>
      {navigationBar}
      <NavigationContainer
        independent={true}
      >
        <Tab.Navigator

          screenOptions={(route) => ({
            tabBarStyle: styles.tabStyle,
            tabBarScrollEnabled: true, //是否支持 选项卡滚动，默认false
            tabBarActiveTintColor: 'white',
            tabBarContentContainerStyle: {
              backgroundColor: theme
            },
            tabBarIndicatorStyle: styles.indicatorStyle,
            tabBarLabelStyle: styles.labelStyle,//文字的样式
            lazy: true,
          })}
        >
          {
            Object.entries(this._genTabs()).map((item,index) => {
              return <Tab.Screen
                name={item[0]}
                component={item[1].screen}
                options={item[1].navigationOptions}
                key = {index}
              />;
            })
          }

        </Tab.Navigator>


      </NavigationContainer>
      {this.renderTrendingDialog()}
    </View>




  }


  right() {

    return <TouchableOpacity
      style={{paddingRight: 8}}
      onPress={()=> {
        NavigationUtil.navigation.navigate("AboutPage")
      }}
    >
      <Feather
        name={'award'}
        size={26}
        style={{color: 'white'}}/>
    </TouchableOpacity>
  }

  // left(){
  //   return <TouchableOpacity
  //     style={{paddingLeft: 0}}
  //     // onPress={callBack}
  //   >
  //     <Feather
  //       name={'chevron-left'}
  //       size={32}
  //       style={{color: 'white'}}/>
  //   </TouchableOpacity>
  // }


}


const mapTrendingStateToProps = state => ({
  theme: state.theme.theme,
});

export default connect(mapTrendingStateToProps)(TrendingPage);


const pageSize = 10; //设为常量,防止修改
class TrendingTab extends Component<Props> {

  constructor(props) {
    super(props);
    const {tabLabel} = this.props;
    this.storeName = tabLabel;
    this.toastRef = React.createRef();
  }

  //组件完成装载时componentDidMount
  componentDidMount() {
    this.loadData();

  }


  loadData = (loadMore) =>{
    const {onRefreshTrending,onLoadMoreTrending} = this.props;
    const store = this._store();
    const url = this.genFetchUrl(this.storeName);
    if (loadMore){
      onLoadMoreTrending(this.storeName, ++store.pageIndex,pageSize ,store.items,favoriteDao,callback=>{
        this.toastRef.show('没有更多了')
      })
    }else {
      onRefreshTrending(this.storeName, url ,pageSize,favoriteDao);
    }



  }

  /**
   * 获取与当前页面有关的数据
   * @returns {*}
   * @private
   */
  _store = () => {

    const {trending} = this.props;
    let store = trending[this.storeName];
    if (!store) {
      store = {
        items: [],
        isLoading: true,
        projectModels: [],//要显示的数据
        hideLoadingMore: true,//默认隐藏加载更多
      };
    }
    // console.log('_store')
    // console.log(store)
    return store;
  }


  genFetchUrl(key) {
    return URL + key + QUERY_STR;
  }

  renderItem(data) {
    const item = data.item;
    // console.log('呵呵了')
    // console.log(item);
    return <TrendingItem
      projectModel = {item}
      onSelect = {(callback)=>{
        NavigationUtil.goPage("DetailPage",{
          projectModel: item,
          flag: FLAG_STORAGE.flag_trending,
          // callback,
        })

      }}s
      onFavorite={(item, isFavorite) => {
        FavoriteUtil.onFavorite(favoriteDao,item,isFavorite,FLAG_STORAGE.flag_popular)
      }}

    >

    </TrendingItem>
  }

  genIndicator() {
    return this._store().hideLoadingMore ? null :
      <View style={styles.indicatorContainer}>
        <ActivityIndicator
          style={styles.indicator}
        />
        <Text>正在加载更多</Text>
      </View>;
  }

  render() {
    // const {popular} = this.props;
    let store = this._store();
    return <View style={styles.container}>
      <FlatList
        data={store.projectModels}
        renderItem={data => this.renderItem(data)}
        keyExtractor={item => '' + item.item.id}
        scrollEventThrottle={50}
        removeClippedSubviews={false}
        ListEmptyComponent={()=>{
          return <View style={{flex: 1,backgroundColor: "yellow"}}></View>
        }}
        refreshControl={
          <RefreshControl
            title={'Loading'}
            titleColor={THEME_COLOR}
            colors={[THEME_COLOR]}
            refreshing={store.isLoading}
            onRefresh={() => this.loadData(false)}
            tintColor={THEME_COLOR}
            progressViewOffset={30}
          />
        }
        ListFooterComponent={() => this.genIndicator()}
        onEndReached={() => {
          console.log('---onEndReached----');
          setTimeout(()=>{
            if (this.canLoadMore){
              this.loadData(true)
              this.canLoadMore = false
            }
          },100);

        }}
        onEndReachedThreshold={0.5}
        onMomentumScrollBegin={() => {
          this.canLoadMore = true; //fix 初始化时页调用onEndReached的问题
          console.log('---onMomentumScrollBegin-----');
        }}
      />
      <Toast
        ref={ref => this.toastRef = ref}
        position={'center'}
      ></Toast>
    </View>;
  }

}

const mapStateToProps = state => ({
  trending: state.trending,
});
const mapDispatchToProps = dispatch => ({
  //将 dispatch(onRefreshPopular(storeName, url))绑定到props
  onRefreshTrending: (storeName, url, pageSize,favoriteDao) => dispatch(action.onRefreshTrending(storeName, url,pageSize,favoriteDao)),
  onLoadMoreTrending: (storeName, pageIndex, pageSize, items, favoriteDao,callBack) => dispatch(action.onLoadMoreTrending(storeName, pageIndex, pageSize, items,favoriteDao, callBack)),
  // onFlushPopularFavorite: (storeName, pageIndex, pageSize, items, favoriteDao) => dispatch(action.onFlushPopularFavorite(storeName, pageIndex, pageSize, items, favoriteDao)),
});

const TrendingTabPage = connect(mapStateToProps, mapDispatchToProps)(TrendingTab);


const styles = StyleSheet.create({
  container:{
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});
