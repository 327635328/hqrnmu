import React, {useEffect, useLayoutEffect} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import Service from "./serviceIndex";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {scale} from 'react-native-size-matters';
import BodybuildIndex from "./bodybuildIndex";
import socialCircleIndex from "./socialCircleIndex";
import mallIndex from "./mallIndex";
import mineIndex from "./mineIndex";
import { useDispatch, useSelector } from "react-redux";
import {changeLoginInfo, changeUserInfo} from '../../store/user/actions';
const TabBarItems = {
  Service: {
    tabName: '健康',
    component: Service,
    activeIcon: require('../../assets/images/icon-footerbar-01-active.png'),
    inactiveIcon: require('../../assets/images/icon-footerbar-01.png'),
  },
  Motion: {
    tabName: '运动',
    component: BodybuildIndex,
    activeIcon: require('../../assets/images/icon-footerbar-02-active.png'),
    inactiveIcon: require('../../assets/images/icon-footerbar-02.png'),
  },
  SocialCircle: {
    tabName: '发现',
    component: socialCircleIndex,
    activeIcon: require('../../assets/images/icon-footerbar-03-active.png'),
    inactiveIcon: require('../../assets/images/icon-footerbar-03.png'),
  },
  Mall: {
    tabName: '好物',
    component: mallIndex,
    activeIcon: require('../../assets/images/icon-footerbar-04-active.png'),
    inactiveIcon: require('../../assets/images/icon-footerbar-04.png'),
  },
  Mine: {
    tabName: '我的',
    component: mineIndex,
    activeIcon: require('../../assets/images/icon-footerbar-05-active.png'),
    inactiveIcon: require('../../assets/images/icon-footerbar-05.png'),
  },
};
const Tab = createBottomTabNavigator();
export const HomeComponent = ({navigation, route}) => {

  const dispatch = useDispatch();



  useLayoutEffect(() => {
    global.navigation = navigation; // app需要用其重定向至登录页面
  }, [navigation]);

  const onTabPress = ({navigation, route}) => {
    if (navigation.isFocused()) {

    }
  };
  return (
    <Tab.Navigator
      // lazy={true}
      screenOptions={({route}) => ({
        tabBarActiveTintColor:"#2AD1B6",
        tabBarHideOnKeyboard: true,
        tabBarInactiveTintColor: '#666',
        headerShown:false,
        // tabBarStyle:{height: scale(50)},
        // tabBarLabelStyle: {
        //   height: scale(20),
        //   textAlignVertical: 'top',
        //   fontSize: scale(9),
        // },
        lazy:true,
        tabBarIcon: ({focused, color, size}) => {
          let tab = TabBarItems[route.name];
          // console.log('//////////////', tab);
          if (!tab) return null;
          return (
            <Image
              style={{marginTop: scale(5), width: scale(24), height: scale(24)}}
              source={focused ? tab.activeIcon : tab.inactiveIcon}
            />
          );
        },
      })}

      initialRouteName="Service"
    >
      {Object.keys(TabBarItems).map((tab, index) => {
        let tabItem = TabBarItems[tab];
        // console.log('qqqqqqqqqqqqq--', tabItem.tabName);
        return (
          <Tab.Screen
            key={index}
            listeners={onTabPress}
            name={tab}
            component={tabItem.component}
            options={{
              tabBarLabel: tabItem.tabName,
            }}
          />
        );
      })}
    </Tab.Navigator>
  );
}


const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: "red",
    // justifyContent: "center",
    // alignItems: "center",
  },
  text:{
    fontSize: 15,
    color: "red",
  },
  tabStyle: {
    // minWidth: 50 //fix minWidth会导致tabStyle初次加载时闪烁
    padding: 0,
  },
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  labelStyle: {
    fontSize: 13,
    margin: 0,
  },
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});
